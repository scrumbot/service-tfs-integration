FROM maven:3.5.3-jdk-8-alpine as target
WORKDIR /build
COPY pom.xml .

RUN mvn dependency:go-offline

COPY src/ /build/src/
RUN mvn package -DskipTests

FROM openjdk:8-jre-alpine
EXPOSE 8000
COPY --from=target /build/target/service-tfsintegration.jar /app/service-tfsintegration.jar

CMD exec java -jar -Djava.security.egd=file:/dev/./urandom /app/service-tfsintegration.jar