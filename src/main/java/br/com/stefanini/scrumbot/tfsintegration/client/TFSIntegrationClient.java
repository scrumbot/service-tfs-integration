package br.com.stefanini.scrumbot.tfsintegration.client;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.stefanini.scrumbot.tfsintegration.dto.BacklogResquestDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.SprintAtualDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.WorkItemByDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.WorkItemFieldsDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.WorkItemIdDTO;
import br.com.stefanini.scrumbot.tfsintegration.enums.TFSUrl;
import br.com.stefanini.scrumbot.tfsintegration.exception.BusinessException;
import br.com.stefanini.scrumbot.tfsintegration.util.HttpUtils;
import br.com.stefanini.scrumbot.tfsintegration.util.TFSJsonUtils;

@Component
public class TFSIntegrationClient {

	private static final String NOME_PROJETO = "@NomeProjeto";

	private RestTemplate restTemplate = new RestTemplate();

	public List<WorkItemFieldsDTO> obterBacklogs(BacklogResquestDTO resquest) {

		String ids = obterBacklogsIds(resquest);

		WorkItemByDTO workitemById = restTemplate.exchange(resquest.getColecao() + TFSUrl.BACKLOG_BY_ID.getUrl(),
				HttpMethod.GET, new HttpEntity<Object>(createHeaders(resquest.getToken())), WorkItemByDTO.class, ids)
				.getBody();

		return workitemById.getWorkItems().stream().map(w -> w.getFields()).collect(Collectors.toList());
	}

	public String obterBacklogsIds(BacklogResquestDTO resquest) {

		tratarParametros(resquest);
		WorkItemIdDTO workitem = restTemplate.postForObject(resquest.getColecao() + TFSUrl.BACKLOG_BY_QUERY.getUrl(),
				new HttpEntity<String>(TFSJsonUtils.criarJsonConsultaWit(resquest.getConsulta()),
						createHeaders(resquest.getToken())),
				WorkItemIdDTO.class);

		return workitem.getWorkitems().stream().map(w -> w.getId()).collect(Collectors.joining(","));
	}

	public String obterSprintAtual(BacklogResquestDTO resquest) {

		SprintAtualDTO sprint = restTemplate.exchange(resquest.getColecao() + TFSUrl.CURRENT_SPRINT.getUrl(),
				HttpMethod.GET, new HttpEntity<Object>(createHeaders(resquest.getToken())), SprintAtualDTO.class,
				resquest.getNomeProjeto()).getBody();

		if (sprint == null || sprint.getQuantidade() <= 0) {
			throw new BusinessException("Não existe uma iteração atual.");
		}

		return extrairNomeSprint(sprint, resquest.getNomeProjeto());
	}

	private void tratarParametros(BacklogResquestDTO resquest) {
		if (resquest.getConsulta().contains(NOME_PROJETO)) {
			resquest.setConsulta(resquest.getConsulta().replaceAll(NOME_PROJETO, resquest.getNomeProjeto()));
		}
	}

	private String extrairNomeSprint(SprintAtualDTO sprint, String nomeProjeto) {
		return sprint.getSprints().get(0).getNome().replace(nomeProjeto, StringUtils.EMPTY);
	}

	private HttpHeaders createHeaders(String token) {
		return HttpUtils.createHeadersWithBasicAuthentication(token);

	}
}
