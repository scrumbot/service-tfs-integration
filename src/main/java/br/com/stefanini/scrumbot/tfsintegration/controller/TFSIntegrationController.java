package br.com.stefanini.scrumbot.tfsintegration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.scrumbot.tfsintegration.dto.BacklogResquestDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.WorkItemFieldsDTO;
import br.com.stefanini.scrumbot.tfsintegration.service.TFSIntegrationService;

@RestController
@RequestMapping(value = "/tfs")
public class TFSIntegrationController {

	@Autowired
	private TFSIntegrationService tfsIntegrationService;
	
	@PostMapping("/backlogs")
	public List<WorkItemFieldsDTO> iniciar(@RequestBody BacklogResquestDTO request) {
		
		return tfsIntegrationService.obterBacklogs(request);
	}
}
