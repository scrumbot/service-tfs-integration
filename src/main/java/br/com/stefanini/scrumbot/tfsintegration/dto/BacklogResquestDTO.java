package br.com.stefanini.scrumbot.tfsintegration.dto;

public class BacklogResquestDTO {
		
	private String token;
	private String consulta;
	private String colecao;
	private String nomeProjeto;

	public BacklogResquestDTO(String token, String consulta, String colecao, String nomeProjeto) {
		super();
		this.token = token;
		this.consulta = consulta;
		this.colecao = colecao;
		this.nomeProjeto = nomeProjeto;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getColecao() {
		return colecao;
	}

	public void setColecao(String colecao) {
		this.colecao = colecao;
	}

	public String getNomeProjeto() {
		return nomeProjeto;
	}

	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}

}
