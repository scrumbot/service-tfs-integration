package br.com.stefanini.scrumbot.tfsintegration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdWorkItemDTO {

	@JsonProperty(value = "id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
