package br.com.stefanini.scrumbot.tfsintegration.dto;

import java.time.LocalDateTime;

public class ProjetoDTO {

	private Integer id;
	private String nome;
	private String descricao;
	private String colecao;
	private String tokenAcesso;
	private String tokenBot;
	private LocalDateTime dataCricacao;
	private boolean ativo;
	
	public ProjetoDTO() {
		super();
	}

	public ProjetoDTO(Integer id, String nome, String descricao, String colecao, String tokenAcesso, String tokenBot,
			LocalDateTime dataCricacao, boolean ativo) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.colecao = colecao;
		this.tokenAcesso = tokenAcesso;
		this.tokenBot = tokenBot;
		this.dataCricacao = dataCricacao;
		this.ativo = ativo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getColecao() {
		return colecao;
	}

	public void setColecao(String colecao) {
		this.colecao = colecao;
	}

	public String getTokenAcesso() {
		return tokenAcesso;
	}

	public void setTokenAcesso(String tokenAcesso) {
		this.tokenAcesso = tokenAcesso;
	}

	public String getTokenBot() {
		return tokenBot;
	}

	public void setTokenBot(String tokenBot) {
		this.tokenBot = tokenBot;
	}

	public LocalDateTime getDataCricacao() {
		return dataCricacao;
	}

	public void setDataCricacao(LocalDateTime dataCricacao) {
		this.dataCricacao = dataCricacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
