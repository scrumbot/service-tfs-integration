package br.com.stefanini.scrumbot.tfsintegration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SprintAtualDTO {

	@JsonProperty(value = "count")
	private Integer quantidade;

	@JsonProperty(value = "value")
	private List<SprintDTO> sprints;

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public List<SprintDTO> getSprints() {
		return sprints;
	}

	public void setSprints(List<SprintDTO> sprints) {
		this.sprints = sprints;
	}

}
