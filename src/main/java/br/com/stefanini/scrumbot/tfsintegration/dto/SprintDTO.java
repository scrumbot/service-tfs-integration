package br.com.stefanini.scrumbot.tfsintegration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SprintDTO {

	@JsonProperty(value = "path")
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
