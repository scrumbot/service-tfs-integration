package br.com.stefanini.scrumbot.tfsintegration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkItemByDTO {

	@JsonProperty(value = "count")
	private Integer quantidade;
	
	@JsonProperty(value = "value")
	private List<WorkItemDTO> workItems;

	public List<WorkItemDTO> getWorkItems() {
		return workItems;
	}

	public void setWorkItems(List<WorkItemDTO> workItems) {
		this.workItems = workItems;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	

}
