package br.com.stefanini.scrumbot.tfsintegration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkItemDTO {

	@JsonProperty(value = "id")
	private String id;
	
	@JsonProperty(value = "url")
	private String url;
	
	@JsonProperty(value = "fields")
	private WorkItemFieldsDTO fields;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WorkItemFieldsDTO getFields() {
		return fields;
	}

	public void setFields(WorkItemFieldsDTO fields) {
		this.fields = fields;
	}
	


}
