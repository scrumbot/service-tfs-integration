package br.com.stefanini.scrumbot.tfsintegration.dto;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkItemFieldsDTO {

	@JsonProperty(value = "System.Title")
	private String title;
	
	@JsonProperty(value = "System.State")
	private String state;
	
	@JsonProperty(value = "System.AssignedTo")
	private String assignedTo;
	
	@JsonProperty(value = "Microsoft.VSTS.Scheduling.OriginalEstimate")
	private String originalEstimate;
	
	@JsonProperty(value = "Microsoft.VSTS.Scheduling.CompletedWork")
	private String completedWork;
	
	@JsonProperty(value = "Microsoft.VSTS.Scheduling.RemainingWork")
	private String remainingWork;
	
	public String getTitle() {
		if (title != null) {
			return title;
		}
		return StringUtils.EMPTY;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		if (state != null) {
			return state;
		}
		return StringUtils.EMPTY;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAssignedTo() {
		if (assignedTo != null) {
			return assignedTo;
		}
		return StringUtils.EMPTY;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getOriginalEstimate() {
		if (originalEstimate != null) {
			return originalEstimate;
		}
		return StringUtils.EMPTY;
	}

	public void setOriginalEstimate(String originalEstimate) {
		this.originalEstimate = originalEstimate;
	}

	public String getCompletedWork() {
		if (completedWork != null) {
			return completedWork;
		}
		return StringUtils.EMPTY;
	}

	public void setCompletedWork(String completedWork) {
		this.completedWork = completedWork;
	}

	public String getRemainingWork() {
		if (remainingWork != null) {
			return remainingWork;
		}
		return StringUtils.EMPTY;
	}

	public void setRemainingWork(String remainingWork) {
		this.remainingWork = remainingWork;
	}
	
	@Override
	public String toString() {
		return "Title: " + getTitle() + "\n State: " + getState() + "\n AssignedTo: " + getAssignedTo()
				+ "\n OriginalEstimate: " + getOriginalEstimate() + "\n CompletedWork: " + getCompletedWork()
				+ "\n RemainingWork: " + getRemainingWork() + "\n";
	}

}
