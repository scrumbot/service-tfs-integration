package br.com.stefanini.scrumbot.tfsintegration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkItemIdDTO {

	@JsonProperty(value = "workItems")
	private List<IdWorkItemDTO> workitems;

	public List<IdWorkItemDTO> getWorkitems() {
		return workitems;
	}

	public void setWorkitems(List<IdWorkItemDTO> workitems) {
		this.workitems = workitems;
	}
	
}
