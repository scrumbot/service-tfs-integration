package br.com.stefanini.scrumbot.tfsintegration.enums;

public enum TFSUrl {
	
	CURRENT_SPRINT("/{projeto}/_apis/work/teamsettings/iterations?api-version=4.0&$timeframe=current"),
	BACKLOG_BY_QUERY("/_apis/wit/wiql?api-version=4.0"),
	BACKLOG_BY_ID("/_apis/wit/workitems?ids={ids}&api-version=4.0");

	private String url;
	
	private TFSUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return this.url;
	}
}
