package br.com.stefanini.scrumbot.tfsintegration.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.stefanini.scrumbot.tfsintegration.client.TFSIntegrationClient;
import br.com.stefanini.scrumbot.tfsintegration.dto.BacklogResquestDTO;
import br.com.stefanini.scrumbot.tfsintegration.dto.WorkItemFieldsDTO;

@Service
public class TFSIntegrationService {

	@Autowired
	private TFSIntegrationClient tfsIntegrationClient;

	public List<WorkItemFieldsDTO> obterBacklogs(BacklogResquestDTO resquest) {
		return tfsIntegrationClient.obterBacklogs(resquest);
	}
}
