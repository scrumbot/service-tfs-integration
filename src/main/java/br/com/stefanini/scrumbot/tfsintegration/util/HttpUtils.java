package br.com.stefanini.scrumbot.tfsintegration.util;

import org.springframework.http.HttpHeaders;

public class HttpUtils {
	
	private static final String APPLICATION_JSON = "application/json";
	private static final String BASIC = "Basic ";
	private static final String AUTHORIZATION2 = "Authorization";

	public static HttpHeaders createHeadersWithBasicAuthentication(String token) {
		HttpHeaders header = new HttpHeaders();
		header.add(AUTHORIZATION2, BASIC + token);
		header.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON);
		return header;

	}
}
